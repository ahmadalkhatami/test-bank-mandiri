# Test Bank Mandiri

## Auction

Aplikasi backend lelang yang dibangun menggunakan arsitektur microservices.

## Description

Auction adalah mini project bertema lelang, yang dirancang untuk menangani lalu lintas tinggi dan skenario lelang yang kompleks dengan efisiensi dan keandalan. Aplikasi ini memanfaatkan beberapa teknologi terbaru untuk menyediakan sistem backend yang kuat dan dapat diskalakan.

## Teknologi:

.NET Core 8: Kerangka kerja utama untuk membangun aplikasi lintas platform dengan kinerja tinggi.
.Duende Identity Server: Menerapkan OAuth2 dan OpenID Connect untuk otentikasi dan otorisasi yang aman.
.YARP (Yet Another Reverse Proxy): Mengelola reverse proxy untuk merutekan permintaan ke microservices yang sesuai.
.RabbitMQ: Broker pesan yang andal untuk menangani komunikasi asinkron antar microservices.
.MongoDB: Basis data NoSQL yang dioptimalkan untuk fitur search pada search-svc.
.PostgreSQL: Basis data relasional open-source yang kuat untuk manajemen data terstruktur.
.Docker: Aplikasi dijalankan diatas container yang sudah di konfigurasi agar tidak perlu menjalankan service satu persatu.

## Fitur:

.Arsitektur Microservices: Setiap fitur diimplementasikan sebagai microservice terpisah, yang mendukung skalabilitas dan pemeliharaan.
.OAuth2 dan OpenID Connect: Otentikasi dan otorisasi yang aman menggunakan Duende Identity Server.
.Reverse Proxy dengan YARP: Merutekan permintaan klien secara efisien ke berbagai microservices.
.Komunikasi Asinkron: Menggunakan RabbitMQ untuk menangani komunikasi antar microservices.
.Penyimpanan Data yang Fleksibel: Menggabungkan kekuatan MongoDB dan PostgreSQL untuk menangani berbagai kebutuhan data.
.Deployment dengan Kontainerisasi: Docker memastikan aplikasi berjalan lancar di berbagai lingkungan.

## How to:

## Prasyarat :

Harus memiliki tech stack sebagai berikut untuk memulai project :
..NET Core 8 SDK
.Docker
.RabbitMQ
.MongoDB
.PostgreSQL

## Instalasi

clone repo:
1.HTTPS : git clone https://gitlab.com/ahmadalkhatami/test-bank-mandiri.git
2.cd Auction
3.docker compose up -d
4.Test end point dengan Postman karena saya tidak set input bearer tokennya di swagger.
